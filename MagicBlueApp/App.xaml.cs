﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using MagicBlueApp.Services;
using MagicBlueApp.Views;
using MagicBlueApp.Data;
using MagicBlueApp.Models;

namespace MagicBlueApp
{
    public partial class App : Application
    {
        public static string FilSqlitePath;
        public static AppUserModel CurrenUser;

        public App()
        {
            InitializeComponent();

            DependencyService.Register<MockDataStore>();
            MainPage = new AppShell();
        }

        public App(string fileName)
        {
            InitializeComponent();

            DependencyService.Register<MockDataStore>();

            FilSqlitePath = fileName;

            AppUserManager usermanager = new AppUserManager();

            CurrenUser = usermanager.GetUserData();

            CheckLoginStatus();
        }

        private void CheckLoginStatus()
        {
            
            if(CurrenUser != null && CurrenUser.DeviceLoginCode != null)
            {
                MainPage = new NavigationPage(new MainPage());
            }
            else
            {
                MainPage = new NavigationPage(new LoginPage());
            }
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
