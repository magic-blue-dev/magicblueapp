﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MagicBlueApp.Services
{
    public class DialogService : IDialogService
    {
        public Task Message(string message, string title)
        {
            throw new NotImplementedException();
        }

        public Task<bool> MessageActionBool(string Title, string message)
        {
            throw new NotImplementedException();
        }

        public Task<bool> MessageActionBool(string Title, string message, string button1, string button2)
        {
            throw new NotImplementedException();
        }

        public Task<string> ShowActionSheet(string Title, string cancel, string distroy, string button1, string button2, string button3)
        {
            throw new NotImplementedException();
        }

        public Task<string> ShowActionSheet(string Title, string cancel, string distroy, string button1, string button2)
        {
            throw new NotImplementedException();
        }

        public Task<string> ShowActionSheet(string Title, string cancel, string distroy, params string[] buttons)
        {
            throw new NotImplementedException();
        }

        public Task ShowError(string message, string title, string buttonText, Action afterHideCallback)
        {
            throw new NotImplementedException();
        }

        public Task ShowError(Exception error, string title, string buttonText, Action afterHideCallback)
        {
            throw new NotImplementedException();
        }

        public async Task ShowMessage(string message, string title)
        {
            await Application.Current.MainPage.DisplayAlert(
                title,
                message,
                "OK");
        }

        public Task ShowMessage(string message, string title, string buttonText, Action afterHideCallback)
        {
            throw new NotImplementedException();
        }

        public Task ShowMessageBox(string message, string title)
        {
            throw new NotImplementedException();
        }

        public Task<bool> ShowMessageConfirm(string message, string title, string buttonConfirmText, string buttonCancelText, Action<bool> afterHideCallback)
        {
            throw new NotImplementedException();
        }

        public Task ShowPopupMessage(string message)
        {
            throw new NotImplementedException();
        }
    }
}
