﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MagicBlueApp.Data;
using MagicBlueApp.Models;
using Newtonsoft.Json;
using RestSharp;

namespace MagicBlueApp.Services
{
    public class HttpService<T>
    {
        #region Propiedades
        public IRestResponse StatusServer { get; set; }

        #endregion

        #region Constructor
        public HttpService()
        {

        }
        #endregion

        #region Metodos
        public async Task<DataModel> GetRestServiceDataAsync(string subRuta)
        {
            try
            {
                var rutaCompleta = RutaBase.rutaBase + subRuta;
                string tokenString = "";

                tokenString = App.CurrenUser == null ? "" : App.CurrenUser.Token;

                var cliente = new RestClient(rutaCompleta);
                var requerir = new RestRequest(Method.GET);

                requerir.AddHeader("cache-control", "no-cache");
                requerir.AddHeader("Connection", "keep-alive");
                requerir.AddHeader("Accept-Encoding", "gzip, deflate");
                requerir.AddHeader("AppVersion", RutaBase.AppResidentVersion);
                requerir.AddHeader("Accept", "*/*");
                requerir.AddHeader("Authorization", "Bearer " + tokenString);
                requerir.Timeout = 15000;

                CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
                IRestResponse respuesta = await cliente.ExecuteAsync(requerir, cancellationTokenSource.Token);
                
                StatusServer = respuesta;

                var resultado = JsonConvert.DeserializeObject<DataModel>(respuesta.Content);

                return resultado;
            }
            catch
            {
                return null;
            }
        }

        public async Task<DataModel> PostServiceDataAsync(string subRuta, T datos)
        {
            try
            {
                var rutaCompleta = RutaBase.rutaBase + subRuta;

                string tokenString = "";

                tokenString = App.CurrenUser == null ? "" : App.CurrenUser.Token;

                var json = JsonConvert.SerializeObject(datos);
                var cliente = new RestClient(rutaCompleta);
                var requerir = new RestRequest(Method.POST);
                requerir.AddHeader("Content-Type", "application/json");
                requerir.AddHeader("Authorization", "Bearer " + tokenString);
                requerir.AddHeader("AppVersion", RutaBase.AppResidentVersion);
                requerir.AddParameter("undefined", json, ParameterType.RequestBody);

                requerir.Timeout = 25000;
                CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
                IRestResponse respuesta = await cliente.ExecuteAsync(requerir, cancellationTokenSource.Token);

                //RespuestaServidor(respuesta);
                StatusServer = respuesta;

                var resultado = JsonConvert.DeserializeObject<DataModel>(respuesta.Content);

                return resultado;
            }
            catch
            {
                return null;
            }
        }
        #endregion
    }
}
