﻿using System;
using System.Threading.Tasks;

namespace MagicBlueApp.Services
{
    public interface IDialogService
    {
        Task ShowError(string message, string title, string buttonText, Action afterHideCallback);
        Task ShowError(Exception error, string title, string buttonText, Action afterHideCallback);
        Task ShowMessage(string message, string title);
        Task ShowPopupMessage(string message);
        Task Message(string message, string title);
        Task ShowMessage(string message, string title, string buttonText, Action afterHideCallback);
        Task<bool> ShowMessageConfirm(string message, string title, string buttonConfirmText, string buttonCancelText, Action<bool> afterHideCallback);
        Task ShowMessageBox(string message, string title);
        Task<string> ShowActionSheet(string Title, string cancel, string distroy, string button1, string button2, string button3);
        Task<string> ShowActionSheet(string Title, string cancel, string distroy, string button1, string button2);
        Task<string> ShowActionSheet(string Title, string cancel, string distroy, params string[] buttons);
        Task<bool> MessageActionBool(string Title, string message);
        Task<bool> MessageActionBool(string Title, string message, string button1, string button2);
    }
}
