﻿using System;
using System.Collections.Generic;
using MagicBlueApp.ViewModels;
using MagicBlueApp.Views;
using Xamarin.Forms;

namespace MagicBlueApp
{
    public partial class AppShell : Xamarin.Forms.Shell
    {
        public AppShell()
        {
            InitializeComponent();
            Routing.RegisterRoute(nameof(ItemDetailPage), typeof(ItemDetailPage));
            Routing.RegisterRoute(nameof(NewItemPage), typeof(NewItemPage));
        }

    }
}
