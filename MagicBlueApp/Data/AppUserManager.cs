﻿using System;
using System.Linq;
using MagicBlueApp.Models;
using SQLite;

namespace MagicBlueApp.Data
{
    public class AppUserManager
    {
        public void SaveUserData(AppUserModel model)
        {

            using (SQLiteConnection conn = new SQLiteConnection(App.FilSqlitePath))
            {
                conn.CreateTable<AppUserModel>();
                var rows = conn.Insert(model);
            }

        }

        public AppUserModel GetUserData()
        {
            try
            {


                AppUserModel notifyBadge = null;

                using (SQLiteConnection conn = new SQLiteConnection(App.FilSqlitePath))
                {
                    conn.CreateTable<AppUserModel>();

                    notifyBadge = conn.Table<AppUserModel>().ToList().FirstOrDefault();

                }

                return notifyBadge;

            }
            catch
            {
                return null;
            }

        }

        public void UpdateUserData(AppUserModel model)
        {

            using (SQLiteConnection conn = new SQLiteConnection(App.FilSqlitePath))
            {
                conn.CreateTable<AppUserModel>();

                var row = conn.Update(model);

            }
        }

        public void ClearTable()
        {
            using (SQLiteConnection conn = new SQLiteConnection(App.FilSqlitePath))
            {
                conn.CreateTable<AppUserModel>();
                conn.DeleteAll<AppUserModel>();
            }
        }
    }
}
