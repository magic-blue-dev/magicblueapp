﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace MagicBlueApp.Helpers
{
    public class GenerarHash
    {
        public static string GenerarHashSha256(string cadena)
        {
            using (var sha256 = SHA256.Create())
            {
                var hashBytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(cadena));
                var hashJson = BitConverter.ToString(hashBytes).Replace("-", "");
                return hashJson;
            }
        }
    }
}
