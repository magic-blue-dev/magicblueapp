﻿using System;
using System.Threading.Tasks;
using Plugin.DeviceInfo;

namespace MagicBlueApp.Helpers
{
    public class DeviceCode
    {
        private static string salt = "L@t0RQU3mAd@3s>kA11_A1as$?v1*5.26i";

        public static string GenerarNuevoCode()
        {
            string paraGenerarCodigo = CrossDeviceInfo.Current.Id + salt;

            return  GenerarHash.GenerarHashSha256(paraGenerarCodigo);

        }
    }
}
