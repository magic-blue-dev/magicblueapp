﻿using MagicBlueApp.Data;
using MagicBlueApp.Helpers;
using MagicBlueApp.Models;
using MagicBlueApp.Services;
using MagicBlueApp.Views;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace MagicBlueApp.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        #region Variables
        private AppUserManager appUserManager;
        #endregion

        #region Propiedades
        public string User { get; set; }

        public string Password { get; set; }

        public ICommand LoginCommand { get; set; }

        public ICommand RegisterCommand { get; set; }

        public INavigation Navigation { get; set; }

        public IDialogService Dialog { get; set; }
        #endregion

        #region constructor
        public LoginViewModel(INavigation navigation, IDialogService dialog)
        {
            Dialog = dialog;
            Navigation = navigation;
            appUserManager = new AppUserManager();
            LoginCommand = new Command(OnLoginClicked);
            RegisterCommand = new Command(Register);
        }
        #endregion

        #region Metodos
        private async void OnLoginClicked(object obj)
        {
            try
            {
                var service = new HttpService<AppUserModel>();
                var deviceLoginCode = DeviceCode.GenerarNuevoCode();
                var res = await service.PostServiceDataAsync("Account/userlogin", new AppUserModel
                {
                    UserName = User,
                    Password = Password,
                    DeviceLoginCode = deviceLoginCode
                });

                if(service.StatusServer.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    if(res.State == "OK")
                    {
                        if(res.Data != null)
                        {
                            var tokenM = JsonConvert.DeserializeObject<TokenModel>(res.Data.ToString());

                            appUserManager.ClearTable();

                            appUserManager.SaveUserData(new AppUserModel
                            {
                                DeviceLoginCode = tokenM.DeviceLoginCode,
                                Token = tokenM.Token,
                                UserName = User
                            });

                            Application.Current.MainPage = new NavigationPage(new MainPage());
                        }
                        
                    }
                    else
                    {
                        await Dialog.ShowMessage(res.Message, "Aviso");
                    }
                }
                else
                {

                }

            }
            catch
            {

            }
        }

        public async void Register()
        {
            await Navigation.PushAsync(new RegisterPage());
        }
        #endregion
    }
}
