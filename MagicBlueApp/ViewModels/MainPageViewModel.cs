﻿using System;
using System.Collections.ObjectModel;
using MagicBlueApp.Models;
using MagicBlueApp.Services;
using Newtonsoft.Json;

namespace MagicBlueApp.ViewModels
{
    public class MainPageViewModel : BaseViewModel
    {

        #region Variables
        private ObservableCollection<CredentialModel> credential;
        private bool showEmptyState;
        private bool isloading;

        #endregion

        #region Propiedades
        public bool Isloading
        {
            get => isloading;
            set => SetProperty(ref isloading, value);
        }

        public bool ShowEmptyState
        {
            get => showEmptyState;
            set => SetProperty(ref showEmptyState, value);
        }

        public ObservableCollection<CredentialModel> Credentials
        {
            get => credential;
            set => SetProperty(ref credential, value);
        }
        #endregion

        #region Constructor
        public MainPageViewModel()
        {
            Credentials = new ObservableCollection<CredentialModel>();

            LoadData();

            //Credentials.Add(new CredentialModel
            //{
            //    UIDA = "26871623781628371",
            //    NumberId = "27364",
            //    Name = "Puerta principal"
            //});

            //Credentials.Add(new CredentialModel
            //{
            //    UIDA = "hjfjh65u6565765",
            //    NumberId = "27365",
            //    Name = "Puerta principal"
            //});
        }
        #endregion

        #region Metodos
        public async void LoadData()
        {
            ShowEmptyState = false;
            Isloading = true;

            var service = new HttpService<HomeModel>();

            var res = await service.GetRestServiceDataAsync($"Credentials/{App.CurrenUser.DeviceLoginCode}");

            if (service.StatusServer.StatusCode == System.Net.HttpStatusCode.OK)
            {
                if (res.State == "OK")
                {
                    var home = JsonConvert.DeserializeObject<HomeModel>(res.Data.ToString());

                    if (home.IsDeviceLoginCodeOk)
                    {
                        Credentials = home.Credentials;
                    }
                    else
                    {

                    }
                }
                else
                {

                }
            }
            else if (service.StatusServer.StatusCode == System.Net.HttpStatusCode.NoContent)
            {
                ShowEmptyState = true;
            }
            else
            {

            }

            Isloading = false;
        }
        #endregion
    }
}
