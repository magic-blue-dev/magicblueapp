﻿using System;
using System.Windows.Input;
using MagicBlueApp.Models;
using MagicBlueApp.Services;
using Xamarin.Forms;

namespace MagicBlueApp.ViewModels
{
    public class RegisterPageViewModel
    {
        #region Variables
        #endregion

        #region Propiedades
        public RegisterModel RegisterM { get; set; }
        public ICommand RegisterCommand { get; set; }
        public IDialogService Dialog { get; set; }
        public INavigation Navigation { get; set; }
        #endregion

        #region Constructor
        public RegisterPageViewModel(INavigation navigation, IDialogService dialog)
        {
            Dialog = dialog;
            Navigation = navigation;
            RegisterM = new RegisterModel();
            RegisterCommand = new Command(() => Register());
        }
        #endregion

        #region Metodos
        public async void Register()
        {
            var service = new HttpService<RegisterModel>();

            var res = await service.PostServiceDataAsync("Account/userregister", RegisterM);

            if(service.StatusServer.StatusCode == System.Net.HttpStatusCode.OK)
            {
                if(res.State == "OK")
                {
                    await Dialog.ShowMessage("Registro correcto", "Aviso");

                    await Navigation.PopAsync();
                }
                else
                {
                    await Dialog.ShowMessage(res.Message, "Aviso");
                }
            }
            else
            {

            }
        }
        #endregion
    }
}
