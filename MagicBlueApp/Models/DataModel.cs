﻿using System;
namespace MagicBlueApp.Models
{
    public class DataModel
    {
        public string State { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
    }
}
