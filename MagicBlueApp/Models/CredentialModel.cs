﻿using System;
namespace MagicBlueApp.Models
{
    public class CredentialModel : BaseModel
    {
        public string Name { get; set; }
        public string DeviceId { get; set; }
        public string UIDA { get; set; }
        public string NumberId { get; set; }

    }
}
