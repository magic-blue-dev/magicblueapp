﻿using System;
namespace MagicBlueApp.Models
{
    public class AppUserModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }
        public string DeviceLoginCode { get; set; }
        public string ToenHash { get; set; }
    }
}
