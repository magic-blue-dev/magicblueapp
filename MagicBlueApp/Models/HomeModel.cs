﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace MagicBlueApp.Models
{
    public class HomeModel
    {
        public bool IsDeviceLoginCodeOk { get; set; }
        public ObservableCollection<CredentialModel> Credentials { get; set; }
    }
}
