﻿using System;
namespace MagicBlueApp.Models
{
    public class BaseModel
    {
        public long Id { get; set; }
        public DateTime AddedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public bool Enabled { get; set; }
    }
}
