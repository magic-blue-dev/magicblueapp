﻿using System;
namespace MagicBlueApp.Models
{
    public class TokenModel
    {
        public string Token { get; set; }

        public string DeviceLoginCode { get; set; }
    }
}
