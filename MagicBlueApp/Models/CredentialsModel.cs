﻿using System;
namespace MagicBlueApp.Models
{
    public class CredentialsModel
    {
        public string Name { get; set; }
        public string DeviceId { get; set; }
        public string UIDA { get; set; }
        public string NumberId { get; set; }
    }
}
