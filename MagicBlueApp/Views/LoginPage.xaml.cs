﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MagicBlueApp.Services;
using MagicBlueApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MagicBlueApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        public LoginPage()
        {
            InitializeComponent();

            var dialogService = new DialogService();
            this.BindingContext = new LoginViewModel(Navigation, dialogService);
        }
    }
}
