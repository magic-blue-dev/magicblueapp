﻿using System;
using System.Collections.Generic;
using MagicBlueApp.ViewModels;
using Xamarin.Forms;

namespace MagicBlueApp.Views
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();

            BindingContext = new MainPageViewModel();
        }
    }
}
