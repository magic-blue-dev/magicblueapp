﻿using System.ComponentModel;
using Xamarin.Forms;
using MagicBlueApp.ViewModels;

namespace MagicBlueApp.Views
{
    public partial class ItemDetailPage : ContentPage
    {
        public ItemDetailPage()
        {
            InitializeComponent();
            BindingContext = new ItemDetailViewModel();
        }
    }
}
