﻿using System;
using System.Collections.Generic;
using MagicBlueApp.Services;
using MagicBlueApp.ViewModels;
using Xamarin.Forms;

namespace MagicBlueApp.Views
{
    public partial class RegisterPage : ContentPage
    {
        public RegisterPage()
        {
            InitializeComponent();

            var dialogService = new DialogService();
            BindingContext = new RegisterPageViewModel(Navigation, dialogService);
        }
    }
}
